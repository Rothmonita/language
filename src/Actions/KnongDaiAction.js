
export const  fetchMainCategory = () => {
    return(dispatch)=>{
        fetch('http://www.knongdai.com/api/v1/categories')
        .then(res=>res.json())
        .then(res=>dispatch({
            type:'KNONGDAI_FETCH_MAIN_CATEGORY',
            payload:res
        }))
    }
}
