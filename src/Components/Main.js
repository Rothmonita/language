import { createAppContainer, createDrawerNavigator, createStackNavigator } from 'react-navigation'
import React, { Component } from 'react'
import { View, Text, Button, Image, StyleSheet } from 'react-native'
import Home from './Home'
import SideMenu from './SideMenu'
import Setting from './Setting'
import AboutUS from './AboutUs'

const MyDrawerNavigator = createDrawerNavigator({
    Home: {
        screen: Home,
    },
    Setting: {
        screen: Setting,
    },
    About: {
        screen: AboutUS
    },
    },
    {
        contentComponent: SideMenu,
        drawerWidth: 300
    }
);

const MyStackNavigator = createStackNavigator({
    About: {
        screen: AboutUS
    },
    Setting: {
        screen: Setting
    }
})
const Main = createAppContainer(MyDrawerNavigator, MyStackNavigator);
export default Main