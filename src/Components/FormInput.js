import React, { Component } from "react";
import { Text, StyleSheet } from "react-native";
import {
  Container,
  Content,
  Form,
  Item,
  Input,
  Label,
  Button,
  Card
} from "native-base";

import { RadioGroup, RadioButton } from "react-native-flexi-radio-button";

const options = {
  title: "URL Request",
  takePhotoButtonTitle: "Take photo with your camera",
  chooseFromLibraryButtonTitle: "Choose photo from your gallery"
};

export default class FormInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      avatarSource: null,
      cate: "",
      subcate: ""
    };
  }

  imgClick = () => {
    ImagePicker.showImagePicker(options, response => {
      console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        const source = { uri: response.uri };

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: source
        });
      }
    });
  };

  render() {
    return (     
        <Container>
          <Content>
            <Text style={{fontFamily:'Roboto',fontSize:24,alignSelf:'center',marginTop:20,color:'#F7941D'}}>Enter User's Information</Text>
            <Form style={styles.frm}>
              <Item stackedLabel style={styles.all}>
                <Label style={styles.frm}>User Name</Label>
                <Input placeholder="Enter user name …" style={styles.all} />
              </Item>
              <Text style={styles.des}>Gender</Text>
              <RadioGroup style={styles.ratio}>
                <RadioButton value={"female"} style={styles.frm}>
                  <Text>Female</Text>
                </RadioButton>

                <RadioButton value={"male"} style={styles.frm}>
                  <Text>Male</Text>
                </RadioButton>
              </RadioGroup>

              <Button block warning style={styles.btn}>
                <Text style={{color:'#FFFFFF'}}>Register</Text>
              </Button>
            </Form>
          </Content>
        </Container>
      
    );
  }
}
const styles = StyleSheet.create({
  all: {
    borderColor: "bisque",
    fontFamily: "Roboto",
    fontSize: 13
  },
  allColor: {
    color: "#666666"
  },
  frm: {
    fontSize: 16
  },
  picker: {
    marginLeft: 15,
    borderColor: "bisque"
  },
  des: {
    marginTop: 20,
    marginLeft: 15,
    fontFamily: "Roboto",
    fontSize: 16
  },
  textArea: {
    fontFamily: "Roboto",
    fontSize: 13,
    flex: 1,
    marginTop: 10,
    marginBottom: 20,
    marginLeft: 15
  },
  btn: {
    fontFamily: "Roboto",
    fontSize: 16,
    width: 200,
    alignSelf: "center",
    marginBottom: 10,
    marginTop:10
  },
  ratio: {
    flex: 1,
    flexDirection: "row",
    marginLeft: 10
  }
});
