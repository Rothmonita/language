import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';
import {Text, View,StyleSheet,Platform,Image,} from 'react-native'
import { Content,List,ListItem} from 'native-base';

export default class SideMenu extends Component {
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

render(){
    return(
        <View style={styles.container}>
            <View 
                style={styles.boxHeader}>
                <View style={styles.imgWrapper}>
                  <Image source={require('../Image/kd-circle1.png')} 
                  style={styles.imgSize} 
                  />
                </View>
                <View style={styles.boxName}>
                  <Text style={styles.name}>Sorn Veng Y</Text>
                  <Text style={styles.email}>sornvengy.y@gmail.com</Text>
                </View>
            </View>
            <View style={styles.menuWrapper}>
                <Content>
                    <List>
                        <ListItem onPress={()=>this.props.navigation.closeDrawer()}>
                              <Image 
                                source={require('../Image/icons8-home-filled-100.png')} 
                                style={styles.menuIcon}
                              />  
                              <Text 
                                
                                style={styles.menuText}
                              >HOME
                              </Text>
                        </ListItem>
                        <ListItem>
                              <Image 
                                source={require('../Image/icons8-user-100.png')} 
                                style={styles.menuIcon}
                              />
                              <Text style={styles.menuText}>USER PROFILE</Text>
                            </ListItem>
                            <ListItem>
                              <Image 
                                source={require('../Image/icons8-geography-filled-100.png')} 
                                style={styles.menuIcon}
                              />
                              <Text style={styles.menuText}>REQUEST WEBSITE</Text>
                            </ListItem>
                            <ListItem onPress={()=>this.props.navigation.navigate("Setting")}>
                              <Image 
                                source={require('../Image/icons8-services-480.png')} 
                                style={styles.menuIcon}
                              />
                              <Text                               
                                style={styles.menuText}
                                >SETTING
                              </Text>
                            </ListItem>
                            <ListItem>
                              <Image 
                                source={require('../Image/icons8-login-100.png')} 
                                style={styles.menuIcon}
                              />
                              <Text style={styles.menuText}>LOG IN</Text>
                            </ListItem>
                    </List>
                </Content>
            </View>
            
        </View>


    ) 
}


}

SideMenu.propTypes = {
    navigation: PropTypes.object
  };

const styles = StyleSheet.create({
  container:{
    flex: 1 ,paddingTop:Platform.OS=='ios'?30:0,backgroundColor:'#F7941D'
  }
  ,
  boxHeader:{
    flex:1,justifyContent: 'center',alignItems:'center',padding:20,alignItems: 'center', backgroundColor:'#F7941D'
  },
  imgWrapper:{
    width: 100, height: 100 ,
    backgroundColor:'#fff',
    borderRadius:50,
    borderWidth:1,
    borderColor:'#fff',padding:5
  },
  imgSize:{
    width: "100%", height: "100%" ,
  },
  boxName:{
    justifyContent:'center',alignItems:'center',marginTop:10
  },
  name:{
    color:'white',fontWeight:'bold'
  },
  email:{
    color:'white',fontSize:13,
  },
  menuText:{
    color:'#555555',
    fontSize:13,
    fontWeight: 'bold',
  },
  menuIcon:{
    width:20,
    height:20,
    marginRight: 30
  },
  menuWrapper:{
    flex: 3,backgroundColor:'#fff',paddingTop:10 
  },
})