import React, { Component } from 'react';
import { View, Text, StyleSheet, Platform,Modal, AsyncStorage, Image,Dimensions, TouchableOpacity,TextInput, ScrollView } from 'react-native'
import { Container, Button } from 'native-base';
import { connect } from 'react-redux';
// import { Fetch_Main_Cetegory_Knongdai } from "../Actions/KnongDaiAction"
import { fetchMainCategory } from '../Actions/KnongDaiAction';
import SearchBar from '../ReusableComponent/SearchBar';
import {Actions,Router,Scene} from 'react-native-router-flux'
import { FlatList } from 'react-native-gesture-handler';
import I18n from 'react-native-i18n'
import { chanageLanguage } from '../utilities/other/i18n';


var screen = Dimensions.get('window')

class Home extends Component {
    constructor(){
        super()
        this.state={
            statusModal:false,
            subCate:[
                {id:1,name:'sport1'},
                {id:2,name:'sport12'},
                {id:3,name:'sport3'},
                {id:4,name:'spor4'},
                {id:5,name:'sport5'},
            ],
            language: I18n.locale
        }
    }
    // ****************************{Research Component at home}**************************************
    componentWillUpdate() {
        AsyncStorage.getItem("language")
        .then(lang => {
            chanageLanguage(lang)
            this.setState({language: lang})
        })
    }



    componentDidMount() {
        this.props.fetchMainCategory()
    }
    checkImageUri(imgUrl) {
        if (!imgUrl.includes("http"))
            imgUrl = 'http://www.knongdai.com' + imgUrl;
        return imgUrl;
    }
    GetSectionListItem = (item) => {
    }
    render() {
        const { knongdai } = this.props

      
        return (
            <View style={styles.container}>
                {/* Modal sub cate*/}
                <Modal
                    visible={this.state.statusModal}
                    onRequestClose={()=>console.warn('This is a close request')}
                >
                    <View
                    style={{marginTop:50,marginRight:15,alignSelf:'flex-end'}}
                    >
                        <Button transparent
                            onPress={()=>this.setState({statusModal:false})}
                        >
                            <Image 
                                source={require('../Image/icons8-multiply-100.png')}
                                style={{width:40,height:40}}
                            />
                        </Button>
                    </View>
                    <View
                     style={styles.modalBox}
                    >
                    <View>
                        <Text style={{fontSize:25}}>Healthy</Text>
                            <Image source={require('../Image/logo-KD-small.png')}
                            style={{ width: 100, height: 100 }}/>
                        </View>
                        <ScrollView>
                            <View>
                                {this.state.subCate.map(item=>(
                                    <TouchableOpacity
                                    key={item.id}
                                    style={{padding:20,shadowRadius:5,width:'100%',borderBottomWidth:1,borderColor:'rgba(0,0,0,0.3)',flex:1}}
                                    >
                                        <Text>{item.name}</Text>
                                    </TouchableOpacity>
                                ))}
                            </View>
                        </ScrollView>
                    </View>
                </Modal>

                {/* top menu */}
                <View style={styles.menuBar}>
                    <Button transparent
                        onPress={() => this.props.navigation.toggleDrawer()}
                    >
                        <Image source={require('../Image/humburger.png')}
                            style={{ width: 25, height: 25 }}
                        />
                    </Button>
                    <Image
                        source={require('../Image/logo-kd-txt.png')}
                        style={{ width: 120, height: 15, marginTop: 15 }}
                    />
                    <Button transparent
                        onPress={() => this.props.navigation.navigate("About")}
                    >
                        <Image source={require('../Image/icons8-alarm-filled-100.png')}
                            style={{ width: 25, height: 25 }}
                        />
                    </Button>
                </View>
                {/* end top menu */}
                {/* Logo */}
                <View style={{ marginTop: 15 }}>
                    <Image source={require('../Image/logo-KD-small.png')}
                        style={{ width: 100, height: 100 }}
                    />
                </View>
                {/* Search */}
                <SearchBar />
                {/* end Search */}
                {/* category */}
                <ScrollView>
                    <View style={[styles.boxWrapper, styles.shadow]}>
                        {knongdai.map(knongdaiObj => {
                            return (
                                 <TouchableOpacity 
                                onPress={()=>this.setState({statusModal:true})}
                                style={[styles.fifty, styles.box]} key={knongdaiObj.address}>
                                    <View>
                                        {console.log('===> ', this.checkImageUri(knongdaiObj.icon_name))}
                                        <Image

                                            source={knongdaiObj.icon_name ? { uri: this.checkImageUri(knongdaiObj.icon_name) } : ('../Image/icons8-expand-arrow-100.png')}
                                            style={{ width: 65, height: 65, }} />
                                    </View>
                                    <View>
                                        <Text>{knongdaiObj.cate_name}</Text>
                                    </View>
                                    <View style={{ margin: -10 }}>
                                        <Button transparent>
                                            <Image
                                                source={require('../Image/icons8-expand-arrow-100.png')}
                                                style={styles.btnDown} />
                                        </Button>
                                    </View>
                                </TouchableOpacity>
                                
                            )
                        })}
                    </View>
                </ScrollView>
               
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        knongdai: state.knongdais.knongdai
    }
}
export default connect(mapStateToProps, { fetchMainCategory })(Home);

const styles = StyleSheet.create({
    menuBar: {
        backgroundColor: '#F7941D',
        padding: 15,
        paddingTop: Platform.OS === 'ios' ? 38 : 5,
        paddingBottom: 5,
        width: '100%',
        borderRadius: 0,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    fifty: {
        width: '48%'
    },
    boxWrapper: {
        flex: 1, margin: 5, marginTop: 0,
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingBottom: 150,

    },
    box: {
        backgroundColor: "rgba(62, 127, 131,0.1)",
        justifyContent: 'center',
        alignItems: 'center',
        height: 180,
        margin: '1%',
        borderColor: 'black',
        borderRadius: 5,
        borderWidth: Platform.OS === 'android' ? 3 : 0,
    },
    btnDown: {
        width: 18, height: 18, borderColor: '#F7941D', borderWidth: 1, borderRadius: 9
    },
    shadow: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
    },
    modalBox:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        paddingTop:50
    }
})