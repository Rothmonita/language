import React, { Component } from 'react';
import { View, Text,StyleSheet,Image,TouchableOpacity } from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label,Button,Picker,Icon,Textarea } from 'native-base';
 
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import ImagePicker from 'react-native-image-picker';

const options={
  title:'URL Request',
  takePhotoButtonTitle:'Take photo with your camera',
  chooseFromLibraryButtonTitle:'Choose photo from your gallery'
}

export default class FormInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      avatarSource:null,
      cate:'',
      subcate:''
    };
  } 

imgClick = () =>{
  ImagePicker.showImagePicker(options, (response) => {
    console.log('Response = ', response);
  
    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    } else {
      const source = { uri: response.uri };
  
      // You can also display the image using data:
      // const source = { uri: 'data:image/jpeg;base64,' + response.data };
  
      this.setState({
        avatarSource: source,
      });
    }
  });
}

  render() {
    return (
        <Container>
        <Content>
          <Form style={styles.frm}>
            <Item stackedLabel style={styles.all}>
              <Label style={styles.frm}>Website Name</Label>
              <Input placeholder="Enter Website name …" style={styles.all}/>
            </Item>
            <Text style={styles.des}>Website structure</Text>
            <RadioGroup
              style={styles.ratio}
            >     
                <RadioButton value={'webSite'} style={styles.frm}>
                  <Text>Website</Text>
                </RadioButton>
                      
                <RadioButton value={'facebookPage'} style={styles.frm} >
                  <Text>Facebook page</Text>
                </RadioButton>                 
            </RadioGroup>
            <Item stackedLabel style={styles.all}>
              <Label style={styles.frm}>URL Address</Label>
              <Input placeholder="URL Address …" style={styles.all}/>
            </Item>
            <Item stackedLabel style={styles.all}>
              <Label style={styles.frm}>Contact</Label>
              <Input placeholder="Mobile number …" style={styles.all}/>
            </Item>
            <Item stackedLabel style={styles.all}>
              <Label style={styles.frm}>Email</Label>
              <Input placeholder="Info@mail.com" style={styles.all}/>
            </Item>
            <Item stackedLabel style={styles.all}>
              <Label style={styles.frm}>Address</Label>
              <Input placeholder="Address …" style={styles.all}/>
            </Item>
            <Image source={this.state.avatarSource} style={styles.uploadAvatar} />

            <TouchableOpacity style={{backgroundColor:'green',margin:10,padding:10}}
              onPress={this.imgClick}
            >
              <Text style={{color:'#fff'}}>Select Image</Text>
            </TouchableOpacity>

            {/* <Image source={this.state.avatarSource} style={styles.uploadAvatar} /> */}
              <Text style={Object.assign({},styles.frm,styles.des)}>Category of Website</Text>
              <Picker
                selectedValue={this.state.cate}
                style={Object.assign({},styles.picker)}
                onValueChange={(itemValue, itemIndex) => this.setState({ cate: itemValue })}>
                <Picker.Item label="Java" value="java" />
                <Picker.Item label="JavaScript" value="js"/>
              </Picker>

              <Text style={Object.assign({},styles.frm,styles.des)}>Sub category</Text>
              <Picker
                selectedValue={this.state.subcate}
                style={Object.assign({},styles.picker)}
                onValueChange={(itemValue, itemIndex) => this.setState({ subcate: itemValue })}>
                <Picker.Item label="Java" value="java" />
                <Picker.Item label="JavaScript" value="js"/>
              </Picker>

               <Item stackedLabel style={styles.all}>
                <Label>Keyword</Label>
                <Input placeholder="add a tag" style={styles.all}/>
              </Item>              
             
              <Label style={Object.assign({},styles.frm,styles.des)}>Discription</Label>
              <Textarea rowSpan={5} bordered style={Object.assign({},styles.all,styles.textArea)}/>

             <Button block warning style={styles.btn}>
              <Text>REQUEST</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}
const styles=StyleSheet.create({
  all:{
    borderColor:'bisque',
    fontFamily: 'Roboto',
    fontSize: 13,
  },
  allColor:{
    color:'#666666',

  },
  frm:{
    fontSize:16,    
  },
  picker:{
    marginLeft:15,
    borderColor:'bisque'
  },
  des:{
    marginTop:20,
    marginLeft: 15,
    fontFamily:'Roboto',
    fontSize:16,
  },
  textArea:{
    fontFamily:'Roboto',
    fontSize:13,
    flex: 1,
    marginTop:10,
    marginBottom: 20,
    marginLeft:15,
  },
  btn:{
    fontFamily:'Roboto',
    fontSize:16,
    width:200,
    alignSelf: 'center',
    marginBottom:10
  },
  ratio:{
    flex:1,
    flexDirection: 'row',
    marginLeft:10,
  }
})
