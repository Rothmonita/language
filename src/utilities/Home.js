import React, { Component } from 'react'
import { Text, View, Button } from 'react-native'
import I18n from 'react-native-i18n';

import { strings, chanageLanguage } from './other/i18n'

export default class Home extends Component {

  state = {
    language: I18n.locale
  }

  _changeLanguage = (lang) => {
    // alert("Hewllo")
    chanageLanguage(lang)
    this.setState({language: lang})
    //I18n.locale = this.state.language
    
  }

  render() {
    return (
      <View style={{marginTop: 100}}>
        <Text> {strings('login.welcome', {name: 'Nita'})} </Text>
        <Button title="Change to Khmer" onPress={() => this._changeLanguage('km')}/>
        <Button title="Change to English" onPress={(ln) => this._changeLanguage('en')}/>
      </View>
    )
  }
}