import React, { Component } from 'react';
import {TextInput,View,Image,StyleSheet} from 'react-native'
import {Button} from 'native-base'
import {strings} from '../utilities/other/i18n'

class SearchBar extends Component {

    render() {
        return (
        <View style={styles.searchbar}>
            <TextInput
            style={styles.searchbox}
            placeholder={strings('home.search_here')} />
            <Button 
            style={styles.btnsearch}
            transparent>
                <Image 
                    source={require('../Image/icons8-search-90.png')}
                    style={{width:25,height:25}}
                />
            </Button>
        </View>
        );
    }
}

export default SearchBar;
const styles=StyleSheet.create({
    searchbar:{width:'90%',
    flexDirection:'row',
    borderWidth:1,
    borderColor:'rgba(0,0,0,0.2)',
    margin:5,
    borderRadius: 25,
    },
    searchbox:{
        flex:6,marginLeft:20
    },
    btnsearch:{
        alignSelf:'flex-end',marginRight:10
    },
})